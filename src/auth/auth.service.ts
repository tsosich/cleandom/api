import { Injectable } from '@nestjs/common';
import { EmployeesService } from './../employees/employees.service';

@Injectable()
export class AuthService {
    constructor(private employeesService: EmployeesService) {}

    async getEmployee(userinfo) {
        return await this.employeesService.findOne(userinfo.sub);
    }

    async matchRoles(roles, user_roles) {
        user_roles = new Set(user_roles)
        const roles_user_have = new Set([...roles].filter(x => user_roles.has(x)));
        return user_roles.size == roles_user_have.size;
    }
}
