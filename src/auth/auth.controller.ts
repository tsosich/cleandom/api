import { ApiTags } from '@nestjs/swagger';

import {
  Controller,
  Get,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { Response } from 'express';

import { LoginGuard } from './guards/login.guard';
import { Issuer } from 'openid-client';
import { AuthenticatedGuard } from './guards/authenticated.guard';

@ApiTags('auth')
@Controller('')
export class AuthController {

  @UseGuards(LoginGuard)
  @Get('/login')
  login() {}

  @Get('/user')
  @UseGuards(AuthenticatedGuard)
  user(@Request() req) {
    return req.user
  }
  
  @UseGuards(LoginGuard)
  @Get('/callback')
  loginCallback(@Res() res: Response) {
    res.redirect('/hello');
  }
  
  @Get('/logout')
  async logout(@Request() req, @Res() res: Response) {
    const id_token = req.user ? req.user.id_token : undefined;
    req.logout();
    req.session.destroy(async (error: any) => {
      const TrustIssuer = await Issuer.discover(`${process.env.AUTH_URL}/.well-known/openid-configuration`);
      const end_session_endpoint = TrustIssuer.metadata.end_session_endpoint;
      if (end_session_endpoint) {
        res.redirect(end_session_endpoint + 
          '?post_logout_redirect_uri=' + `${process.env.SITE_URL}/` + 
          (id_token ? '&id_token_hint=' + id_token : ''));
      } else {
        res.redirect('/')
      }
    })
  }
}