import { UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy, Client, UserinfoResponse, TokenSet, Issuer } from 'openid-client';
import { AuthService } from './../auth.service';

export const buildOpenIdClient = async () => {
  const TrustIssuer = await Issuer.discover(`${process.env.AUTH_URL}/.well-known/openid-configuration`);
  const client = new TrustIssuer.Client({
    client_id: "cleandom",
    client_secret: "super-secret",
  });
  return client;
};

export class OidcStrategy extends PassportStrategy(Strategy, 'oidc') {
  client: Client;

  constructor(private readonly authService: AuthService, client: Client) {
    super({
      client: client,
      params: {
        redirect_uri: `${process.env.API_URL}/callback`,
        scope: "openid profile",
      },
      passReqToCallback: false,
      usePKCE: true,
    });

    this.client = client;
  }

  async validate(tokenset: TokenSet): Promise<any> {
    const userinfo: UserinfoResponse = await this.client.userinfo(tokenset);

    try {
      const id_token = tokenset.id_token
      const access_token = tokenset.access_token
      const refresh_token = tokenset.refresh_token

      const employee = await this.authService.getEmployee(userinfo);

      const user = {
        id_token,
        access_token,
        refresh_token,
        userinfo,
        employee
      }
      return user;
    } catch (err) {
      throw new UnauthorizedException();
    }
  }
}