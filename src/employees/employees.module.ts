import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Employee, EmployeeSchema } from './schemas/employee.schema';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      { 
        name: Employee.name, 
        useFactory: () => {
          const schema = EmployeeSchema;
          schema.pre('save', function() { console.log('Hello from pre save') });
          return schema;
        },
      }
    ]),
  ],
  controllers: [EmployeesController],
  providers: [EmployeesService],
  exports: [EmployeesService],
})
export class EmployeesModule {}
