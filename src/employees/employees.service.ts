import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Employee, EmployeeDocument } from './schemas/employee.schema';

@Injectable()
export class EmployeesService {
  constructor(@InjectModel(Employee.name) private employeeModel: Model<EmployeeDocument>) {}

  async findAllActive() {
    return await this.employeeModel.find({ is_active: true }).exec();
  }

  async findAll() {
    return await this.employeeModel.find().exec();
  }

  async findOne(id: string): Promise<Employee | undefined> {
    return await this.employeeModel.findById({ _id : id }).exec();
  }
}
