import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type EmployeeDocument = Employee & Document;

@Schema()
export class Employee {
  @Prop()
  name: String;

  @Prop()
  roles: [String];

  @Prop()
  email: String;

  @Prop()
  phone: String;

  @Prop()
  avatar: String;  // image/jpg,base64,{{data}}

  @Prop({select: false})
  password: String;

  @Prop({ default: true })
  is_active: Boolean
}

export const EmployeeSchema = SchemaFactory.createForClass(Employee);