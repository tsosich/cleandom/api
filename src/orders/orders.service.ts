import { Model, ObjectId } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Order, OrderDocument } from './schemas/order.schema';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';

@Injectable()
export class OrdersService {
  types_price: Map<string, number> = new Map([
    ['GENERAL', 3000],
    ['SUPPORTING', 3000],
    ['RENOVATION', 3000],
  ])
  options_price: Map<string, number> = new Map([
    ['area', 0],
    ['windows', 300],
    ['sanitates', 300],
    ['carpets', 300],
  ])

  constructor(@InjectModel(Order.name) private orderModel: Model<OrderDocument>) {}

  async createByGuest(createOrderDto: CreateOrderDto): Promise<Order> {
    const createdOrder = new this.orderModel(createOrderDto);
    createdOrder.cost = this.types_price.get(createdOrder.type) + 
                      + this.options_price.get('windows') * createdOrder.options.windows + 
                      + this.options_price.get('sanitates') * createdOrder.options.sanitates + 
                      + this.options_price.get('carpets') * createdOrder.options.carpets;
    return createdOrder.save();
  }

  async findAll(): Promise<Order[]> {
    return this.orderModel.find().populate('employee').exec();
  }

  async findAllForEmployee(id: string) {
    return this.orderModel.find({ 'employee._id': id }).populate('employee').exec();
  }

  async findOne(id: string) {
    return this.orderModel.findById({_id: id}).populate('employee').exec();
  }

  async update(id: string, updateOrderDto: UpdateOrderDto): Promise<Order> {
    const updatedCatDto = this.orderModel.findByIdAndUpdate(id, updateOrderDto, { new: true }).populate('employee');
    return updatedCatDto;
  }

  async remove(id: string) {
    return this.orderModel.findByIdAndDelete(id).exec();
  }
}
