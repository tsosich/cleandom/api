import { ApiProperty } from '@nestjs/swagger';

export class CreateOptionsDto {
    @ApiProperty()
    area: number;

    @ApiProperty()
    windows: number;

    @ApiProperty()
    sanitates: number;
  
    @ApiProperty()
    carpets: number;
}
