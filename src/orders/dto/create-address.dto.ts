import { ApiProperty } from '@nestjs/swagger';

export class CreateAddressDto {
    @ApiProperty()
    city: string;
    
    @ApiProperty()
    street: string;
    
    @ApiProperty()
    house: number;

    @ApiProperty()
    flat: number;
}
