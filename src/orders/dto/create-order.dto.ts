import { ApiProperty } from '@nestjs/swagger';
import { CreateAddressDto } from './create-address.dto';
import { CreateClientDto } from './create-client.dto';
import { CreateOptionsDto } from './create-options.dto';

export class CreateOrderDto {
    @ApiProperty()
    type: string;

    @ApiProperty()
    options: CreateOptionsDto;

    @ApiProperty()
    client: CreateClientDto;

    @ApiProperty()
    cleaningDateTime: Date;

    @ApiProperty()
    address: CreateAddressDto;

    @ApiProperty({ required: false, default: '' })
    comment: string;

    @ApiProperty({ required: false, default: true })
    need_call: boolean;
}
