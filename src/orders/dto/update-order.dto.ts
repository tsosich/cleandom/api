import { ApiProperty } from "@nestjs/swagger";
import { ObjectId } from "mongoose";
import { Employee } from "./../../employees/schemas/employee.schema";
import { CreateAddressDto } from "./create-address.dto";

export class UpdateOrderDto extends CreateAddressDto {
    @ApiProperty({ required: false })
    employee: ObjectId;

    @ApiProperty({ required: false })
    cost: number;
}
