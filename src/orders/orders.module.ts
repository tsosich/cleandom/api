import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { Order, OrderSchema } from './schemas/order.schema';

@Module({
  imports: [MongooseModule.forFeatureAsync([
    { 
      name: Order.name, 
      useFactory: () => {
        const schema = OrderSchema;
        schema.pre('save', function() { console.log('Hello from pre save') });
        return schema;
      },
    },
  ])],
  controllers: [OrdersController],
  providers: [OrdersService]
})
export class OrdersModule {}
