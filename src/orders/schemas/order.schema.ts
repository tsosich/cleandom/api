import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Employee } from './../../employees/schemas/employee.schema';
import { CreateAddressDto } from '../dto/create-address.dto';
import { CreateClientDto } from '../dto/create-client.dto';
import { CreateOptionsDto } from '../dto/create-options.dto';

export type OrderDocument = Order & Document;

@Schema()
export class Order {
  @Prop()
  createDateTime: Date;

  @Prop()
  cleaningDateTime: Date;

  @Prop()
  status: String;

  @Prop()
  address: CreateAddressDto;

  @Prop()
  client: CreateClientDto;

  @Prop()
  type: string;

  @Prop()
  options: CreateOptionsDto;

  @Prop()
  cost: number;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Employee' })
  employee: mongoose.Schema.Types.ObjectId;

  @Prop()
  comment: string;

  @Prop()
  need_call: boolean;
}

export const OrderSchema = SchemaFactory.createForClass(Order);