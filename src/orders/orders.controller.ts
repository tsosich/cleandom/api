import { Controller, Get, Post, Body, Request, Patch, Param, Delete, UseGuards, Put, ForbiddenException } from '@nestjs/common';
import { ApiBearerAuth, ApiOAuth2, ApiTags } from '@nestjs/swagger';
import { OrdersService } from './orders.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { AuthenticatedGuard } from './../auth/guards/authenticated.guard';

@ApiTags('orders')
@Controller('orders')
@ApiBearerAuth()
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Get()
  @UseGuards(AuthenticatedGuard)
  async findAll(@Request() req) {
    console.log(req.user.employee.roles)
    if (req.user.employee.roles.includes('admin') || req.user.employee.roles.includes('operator')) {
      return await this.ordersService.findAll();
    } else {
      return await this.ordersService.findAllForEmployee(req.user.employee._id);
    }
  }

  @Post()
  async createByGuest(@Body() createOrderDto: CreateOrderDto) {
    return await this.ordersService.createByGuest(createOrderDto);
  }

  @Get(':id')
  @UseGuards(AuthenticatedGuard)
  async findOne(@Request() req, @Param('id') id: string) {
    const order = await this.ordersService.findOne(id);
    if (order.employee != req.user.employee && !req.user.employee.roles.includes('admin')) {
      throw new ForbiddenException();
    }
    return order;
  }

  @Put(':id')
  async update(@Request() req, @Param('id') id: string, @Body() updateOrderDto: UpdateOrderDto) {
    const order = await this.ordersService.findOne(id);
    if (order.employee != req.user.employee && !req.user.employee.roles.includes('admin') && !req.user.employee.roles.includes('operator')) {
      throw new ForbiddenException();
    }
    return await this.ordersService.update(id, updateOrderDto);
  }

  @Delete(':id')
  @UseGuards(AuthenticatedGuard)
  async remove(@Request() req, @Param('id') id: string) {
    if (!req.user.employee.roles.includes('admin')) {
      throw new ForbiddenException();
    }
    return await this.ordersService.remove(id);
  }
}
