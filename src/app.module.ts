import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { OrdersModule } from './orders/orders.module';

import { MongooseModule } from '@nestjs/mongoose';
import { EmployeesModule } from './employees/employees.module';
import { AuthModule } from './auth/auth.module';
// import { UsersModule } from './users/users.module';

import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => {
        const username = configService.get('DB_USERNAME');
        const password = configService.get('DB_PASSWORD');
        const database = configService.get('DB_NAME');
        const host = configService.get('MONGO_HOST');

        console.log(`mongodb://${username}:${password}@${host}/${database}`)
 
        return {
          uri: `mongodb://${username}:${password}@${host}/${database}`,
          useNewUrlParser: true,
        };
      },
      inject: [ConfigService],
    }), 
    OrdersModule, 
    EmployeesModule, 
    AuthModule, 
    // UsersModule, ServicesModule, 
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
