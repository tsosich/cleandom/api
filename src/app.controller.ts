import { Controller, Get, Request } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('hello')
  getHello(@Request() req): string {
    if (req.user) {
      return 'Hello, ' + JSON.stringify(req.user.userinfo) + '! <a href="/logout">Logout</a>';
    } else {
      return this.appService.getHello() + ' <a href="/login">Login</a>';
    }
  }

}
